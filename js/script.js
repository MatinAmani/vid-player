/* elements */
const video = document.getElementById('vid')
const buttons = document.getElementById('btns')
const playButton = document.getElementById('play-btn')
const pauseIcon = document.getElementById('pause-icon')
const volSlider = document.getElementById('vol-slider')
const volIcon = document.getElementById('vol-icon')
const volIconMute = document.getElementById('vol-icon-mute')

/* adding event to elements */
video.addEventListener('mouseenter', showButtons)
buttons.addEventListener('mouseenter', showButtons)
video.addEventListener('mouseleave', hideButtons)
video.addEventListener('click', togglePlay)
playButton.addEventListener('click', togglePlay)
volSlider.addEventListener('input', updateVolume)

/* functions */
function showButtons() {
    buttons.classList.add('show')
}

function hideButtons() {
    buttons.classList.remove('show')
}

function togglePlay() {
    if (video.paused) {
        video.play()
    } else {
        video.pause()
    }
}

function updateVolume({ target }) {
    video.volume = target.value

    if (target.value > 0) {
        console.log('unmute')
    } else {
        console.log('mute')
    }
}
